### Deployment Notes

To use this script, the software tampermonkey must be installed on the end user's machine.
Users should use the URL https://gitlab.com/Talibri/enhanced-inventory/raw/master/index.user.js to install this script.

When a new version is merged into the master branch, this will allow tampermonkey to pull the updated script by selecting the update option within tampermonkey.


### Contributing to this Project

To contribute to this project, you should follow the following workflow:
* Create a new branch for the update/feature you wish to implement (eg. `new-feature-name`). 
* `git pull origin master`, to make sure you are up to date with the development environment.
* Work on update/feature.
* Commit and Push changes to remote branch. (`git push --set-upstream origin new-feature-name`)
* Once ready, submit a Merge Request, for your branch to be merged with `master`. 
* Each merge request will require testing before it can be merged with the `master` branch. 


Feel free to contact @kaine.adams for any concerns.
